*****************
The Color Palette
*****************

At the bottom of the Inkscape window, you can find the **color palette**.

The quickest way to apply a color to an object is to:

#. Select the object with the **Selector tool**.
#. **Left-click** on the color of your choice in the palette.
#. The color will be immediately applied to your object.

You can also click-and-drag the color on your object to use that color
for your object.

Each vector object can have a stroke color and a fill color. To apply a
color to the stroke:

#. Select the object with the **Selector tool**.
#. Hold down :kbd:`Shift` and **left-click** on the color field of
   your choice.
#. The color will immediately be applied to the object's stroke.

If you do not like the colors offered by the current palette, you can
use one of the other palettes Inkscape supplies. To switch the palette:

#. Click on the **tiny triangle** to the right of the palette, near
   the margin of the Inkscape window.
#. Click on the name of another palette.

You will also notice that there are options for changing the palette's
appearance at the top of the palette menu, which allow you to make
the palette more or less tightly arranged.

.. Tip:: **Custom palettes**

   It's also possible to create your own color palette. To do so, create
   an empty text file with the file extension :file:`.gpl`, which contains the
   name of the palette in the first line, and your colors in RGB hexadecimal
   format, each in a separate line. Save this file in the folder
   :file:`/palettes`, which you create in the folder indicated at
   :menuselection:`Edit --> Preferences --> System: User config`.

   Restart Inkscape to see the new palette in the list.

|The fill is light blue, the stroke is dark blue.|

An object with a light blue fill and a dark blue stroke

|The palette at the bottom of the window with the palette options menu|

The colorful palette is located at the bottom of the window. The palette
options menu is unfolded.

|Drag and drop a color from the palette|

The fill color can be dragged from the palette onto the object.

.. |The fill is light blue, the stroke is dark blue.| image:: images/fill_stroke_demo.png
.. |The palette at the bottom of the window with the palette options menu| image:: images/palette_w_menu.png
.. |Drag and drop a color from the palette| image:: images/palette_drag_and_drop.png
