*******
Spirals
*******

|Icon for Spiral Tool| :kbd:`F9` or :kbd:`I`

This geometrical shape isn't needed frequently, but sometimes, it proves
to be very useful.

To draw a spiral, click and drag with the mouse on the canvas. When the left mouse button is released, the spiral will be finished. You will notice two diamond-shaped handles on the spiral.

These handles change the length of the spiral. You can try this out
right on the canvas: Just grab one of the handles and drag it along the
spiral's turns to get the desired number of turns and to make the spiral
larger or smaller.

If you hold down :kbd:`Ctrl` while dragging the handles, the spiral will get
longer (or shorter) in 15° steps.


When you combine pressing :kbd:`Alt` with pulling the inner handle
upwards or downwards, it will change the divergence (tightness) /
convergence (looseness) of the spiral, without changing its overall size.
Dragging upwards makes the turns move toward the outside of the spiral. 
Dragging downwards will make them move closer to its center.

The easiest way to change the number of turns of a spiral quickly by a
large amount is to enter the desired number into the field labelled
:guilabel:`Turns` in the tool controls bar. This will not change the spiral's
diameter.

If you ever feel lost when working with the spiral tool, you can use the
rightmost icon in the tool controls bar to remove all changes and to
reset the spiral to its initial shape.

|Reset Icon (broom)|

|Spiral Tool Variations|

Changing a spiral by its handles. Inner handle + Alt and dragging
downwards make the spiral more divergent. Inner handle + Alt and
dragging upwards lets the spiral converge more. A spiral with a large
number of turns. A spiral with a smaller number of turns.

.. |Icon for Spiral Tool| image:: images/spiral_tool.png
   :width: 50px
   :height: 50px
.. |Reset Icon (broom)| image:: images/reset_broom.png
   :width: 30px
   :height: 30px
.. |Spiral Tool Variations| image:: images/spiral_tool_variations.png
